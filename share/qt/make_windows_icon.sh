#!/bin/bash
# create multiresolution windows icon
ICON_SRC=../../src/qt/res/icons/metrocoin.png
ICON_DST=../../src/qt/res/icons/metrocoin.ico
convert ${ICON_SRC} -resize 16x16 metrocoin-16.png
convert ${ICON_SRC} -resize 32x32 metrocoin-32.png
convert ${ICON_SRC} -resize 48x48 metrocoin-48.png
convert metrocoin-16.png metrocoin-32.png metrocoin-48.png ${ICON_DST}

