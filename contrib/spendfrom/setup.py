from distutils.core import setup
setup(name='metspendfrom',
      version='1.0',
      description='Command-line utility for metrocoin "coin control"',
      author='Gavin Andresen',
      author_email='gavin@metrocoinfoundation.org',
      requires=['jsonrpc'],
      scripts=['spendfrom.py'],
      )
